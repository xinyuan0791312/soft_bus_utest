#include "softbus_base.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


namespace cognition_bus {

class SoftTest : public SoftBusBase
{
public:
        bool call(vector<boost::any>& inputs, vector<boost::any>& results)
        {
            return 0;
        }

};

class MyMock: public SoftTest
{
    public:
        MOCK_METHOD1(_creat_async_thread, bool(vector<boost::any>& inputs));
        MOCK_METHOD1(_get_wait_for, bool(int duration));
};

SoftTest softbustest;
MyMock mock;

TEST(Testasync_call,test0){

    string image_path =  "1 (1).jpg";
    cv::Mat cv_img = cv::imread(image_path, 0);
    vector<boost::any> inputs = {cv_img};
    vector<boost::any> results = {cv_img};
    softbustest.results_ = {cv_img};
    EXPECT_CALL(mock, _creat_async_thread(testing::_)).WillOnce(testing::Return(0));

    EXPECT_CALL(mock, _get_wait_for(10)).WillOnce(testing::Return(1));
    EXPECT_EQ(softbustest.async_call(inputs,results,10),1);
}

TEST(_get_insert_idtest,test0){
    
    EXPECT_EQ(0,softbustest._get_insert_id());
    //softbustest->insert_fut_id_ = 1;
    //EXPECT_EQ(0,softbustest->_get_insert_id());
}

}
int main(int argc, char* argv[])
{
    testing::InitGoogleTest(&argc, argv);
    testing::InitGoogleMock(&argc, argv);
    auto result =  RUN_ALL_TESTS();

    return result;
}
